Кастомный триггер ненаступивших событий для рассылки перечня брошенных корзин админу сайта. 
Подробно описан в прилагаемом документе и на сайте работодателя в моей статье 
https://smartraf.ru/allarticles/razrabotka/kak-napisat-kastomnyy-trigger-dlya-1s-bitriks/

Custom trigger of non-occurring events to send a list of abandoned baskets to the site admin.
Described in detail in the attached document and on the website of my employer in my article.
https://smartraf.ru/allarticles/razrabotka/kak-napisat-kastomnyy-trigger-dlya-1s-bitriks/
