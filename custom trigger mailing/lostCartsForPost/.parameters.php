<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
        "PERIOD" => Array(
            "NAME" => "За какое время рассчитать (минуты)",
            "TYPE" => "STRING",
            "DEFAULT" => "60",
            "PARENT" => "BASE",
        ),
        "ALL_TIME" => Array(
            "NAME" => "Рассчитать за все время в прошлом",
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "PARENT" => "BASE",
        ),
        'CACHE_TIME' => array('DEFAULT' => 120),

	),
);
?>