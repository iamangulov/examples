<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
\Bitrix\Main\Loader::includeModule('sale');
\Bitrix\Main\Loader::includeModule('catalog');
//AddMessage2Log(dmp($arParams));

if(!$arParams["PERIOD"]){
    $arParams["PERIOD"] = 120;
}

$dateTo = new \Bitrix\Main\Type\DateTime;
$dateFrom = new \Bitrix\Main\Type\DateTime;
$dateFrom = $dateFrom->add('-'.$arParams["PERIOD"].' minutes');

$isForOldData = $arParams["ALL_TIME"];


if($isForOldData === "Y")
{
    $filter = array(
        '<MIN_DATE_INSERT' => $dateTo->format(\Bitrix\Main\UserFieldTable::MULTIPLE_DATETIME_FORMAT),
    );
}
else
{
    $filter = array(
        '>MIN_DATE_INSERT' => $dateFrom->format(\Bitrix\Main\UserFieldTable::MULTIPLE_DATETIME_FORMAT),
        '<MIN_DATE_INSERT' => $dateTo->format(\Bitrix\Main\UserFieldTable::MULTIPLE_DATETIME_FORMAT),
    );
}
$filter = $filter + array(
        '!FUSER.USER_ID' => null,
        '=ORDER_ID' => null,
        '=LID' => 's1',
    );
//AddMessage2Log(dmp($filter));
$userListDb = \Bitrix\Sale\Internals\BasketTable::getList(array(
    'select' => array('USER_ID' => 'FUSER.USER_ID', 'EMAIL' => 'FUSER.USER.EMAIL', 'FUSER_USER_NAME' => 'FUSER.USER.NAME'),
    'filter' => $filter,
    'runtime' => array(
        new \Bitrix\Main\Entity\ExpressionField('MIN_DATE_INSERT', 'MIN(%s)', 'DATE_INSERT'),
    ),
    'order' => array('USER_ID' => 'ASC')
));

//$userListDb->addFetchDataModifier(array($this, 'getFetchDataModifier'));

//формируем массив пользователей с брошенными корзинами
$lastCartsUsers = $userListDb->fetchAll();

foreach ($lastCartsUsers as $key => $user) {
    $arProfile = CUser::GetByID($user["USER_ID"]);
    $profile = $arProfile->Fetch();
    $lastCartsUsers[$key]["CLIENT"] = $profile["NAME"] . " " . $profile["LAST_NAME"];
    $lastCartsUsers[$key]["EMAIL"] = $profile["EMAIL"];
    $lastCartsUsers[$key]["PHONE"] = $profile["PERSONAL_MOBILE"];
}

//формируем строку ссылок
$linksToCarts = '<table width="100%">';
foreach ($lastCartsUsers as $value) {
    $linksToCarts = $linksToCarts . '<tr><td>';
    $linksToCarts = $linksToCarts . '<a href="';
    $linksToCarts = $linksToCarts . 'http://salatniza.ru/bitrix/admin/sale_basket.php?set_filter=Y&adm_filter_applied=0&filter_user_id=';
    $linksToCarts = $linksToCarts . $value["USER_ID"];
    $linksToCarts = $linksToCarts . '">';
    if (isset($value["CLIENT"]) && !empty($value["CLIENT"])) {
        $linksToCarts = $linksToCarts . $value["CLIENT"];
    } else {
        $linksToCarts = $linksToCarts . 'no name and surname';
    }
    $linksToCarts = $linksToCarts . '</a>';
    $linksToCarts = $linksToCarts . '</td><td>';
    if (isset($value["EMAIL"]) && !empty($value["EMAIL"])) {
        $linksToCarts = $linksToCarts . '<a href="';
        $linksToCarts = $linksToCarts . 'mailto:';
        $linksToCarts = $linksToCarts . $value["EMAIL"];
        $linksToCarts = $linksToCarts . '">';
        $linksToCarts = $linksToCarts . $value["EMAIL"];
        $linksToCarts = $linksToCarts . '</a>';
    } else {
        $linksToCarts = $linksToCarts . 'no email';
    }
    $linksToCarts = $linksToCarts . '</td><td>';
    $linksToCarts = $linksToCarts . '</td><td>';
    if (isset($value["PHONE"]) && !empty($value["PHONE"])) {
        $linksToCarts = $linksToCarts . '<a href="';
        $linksToCarts = $linksToCarts . 'tel:';
        $linksToCarts = $linksToCarts . str_replace([' ', '(', ')', '-'], '', $value["PHONE"]);
        $linksToCarts = $linksToCarts . '">';
        $linksToCarts = $linksToCarts . $value["PHONE"];
        $linksToCarts = $linksToCarts . '</a>';
    } else {
        $linksToCarts = $linksToCarts . 'no phone';
    }
    $linksToCarts = $linksToCarts . '</td></tr>';

}
$linksToCarts = $linksToCarts . '</table>';

$arResult["TABLE"] = $linksToCarts;

echo $arResult["TABLE"];


