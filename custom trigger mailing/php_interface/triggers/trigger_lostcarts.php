<?
class SenderTriggerLostCarts extends \Bitrix\Sender\TriggerConnectorClosed
{

    /*
     * @return string
     *
     * Название триггера
     */
    public function getName()
    {
        return 'Последние брошенные корзины';
    }

    /*
     * @return string
     *
     * Уникальный код триггера
     */
    public function getCode()
    {
        return "yam_last_lost_carts";
    }

    /*
     * @return bool
     *
     * Может ли триггер использоваться как цель,
     * а не только для запуска
     */
    public static function canBeTarget()
    {
        return false;
    }

    /*
     * @return bool
     *
     * Может ли триггер обрабатывать старые данные,
     */
    public static function canRunForOldData()
    {
        return true;
    }

    /*
     * @return bool
     *
     * Функция, которая сообщает, запускать ли рассылку для данного события.
     *
     */
    public function filter()
    {
        \Bitrix\Main\Loader::includeModule('sale');
        \Bitrix\Main\Loader::includeModule('catalog');

        $minutesBasketForgotten = $this->getFieldValue('MINUTES_BASKET_FORGOTTEN');
        if(!is_numeric($minutesBasketForgotten))
            $minutesBasketForgotten = 120; //два часа по умолчанию, можно изменить в настройках триггера

        $dateTo = new \Bitrix\Main\Type\DateTime;
        $dateFrom = new \Bitrix\Main\Type\DateTime;
        $dateFrom = $dateFrom->add('-'.$minutesBasketForgotten.' minutes');

        if($this->isRunForOldData())
        {
            $filter = array(
                '<MIN_DATE_INSERT' => $dateTo->format(\Bitrix\Main\UserFieldTable::MULTIPLE_DATETIME_FORMAT),
            );
        }
        else
        {
            $filter = array(
                '>MIN_DATE_INSERT' => $dateFrom->format(\Bitrix\Main\UserFieldTable::MULTIPLE_DATETIME_FORMAT),
                '<MIN_DATE_INSERT' => $dateTo->format(\Bitrix\Main\UserFieldTable::MULTIPLE_DATETIME_FORMAT),
            );
        }
        $filter = $filter + array(
                '!FUSER.USER_ID' => null,
                '=ORDER_ID' => null,
                '=LID' => $this->getSiteId(),
            );

        $userListDb = \Bitrix\Sale\Internals\BasketTable::getList(array(
            'select' => array('USER_ID' => 'FUSER.USER_ID', 'EMAIL' => 'FUSER.USER.EMAIL', 'FUSER_USER_NAME' => 'FUSER.USER.NAME'),
            'filter' => $filter,
            'runtime' => array(
                new \Bitrix\Main\Entity\ExpressionField('MIN_DATE_INSERT', 'MIN(%s)', 'DATE_INSERT'),
            ),
            'order' => array('USER_ID' => 'ASC')
        ));

        if($userListDb->getSelectedRowsCount() > 0)
        {
            //здесь только проверка - если есть брошенные корзины, отправляем письмо, если нет - не отправляем. Сам список теперь в компоненте в коде письма

            //получателя прописываем единственного - администратора или оператора
            $this->recipient = $this->getRecipient();

            return true;
        }
        else
            return false;
    }

    /*
	 * @return string
	 *
	 * Форма настройки триггера
	 */
    public function getForm()
    {
        $minutesBasketForgottenInput = ' <input size=3 type="text" name="'.$this->getFieldName('MINUTES_BASKET_FORGOTTEN').'" value="'.htmlspecialcharsbx($this->getFieldValue('MINUTES_BASKET_FORGOTTEN', 200000)).'"> ';

        return '
            <table>
                <tr>
                    <td>Сколько минут назад:</td>
                    <td>'.$minutesBasketForgottenInput.'</td>
                </tr>
            </table>
        ';
    }

    /*
     * @return array|\Bitrix\Main\DB\Result|\CDBResult
     *
     * Функция
     * вернет данные о получателе рассылки
     */
    public function getRecipient()
    {
        return array(
            0 => array(
                'NAME' => 'order',
                'EMAIL' => 'order@salatniza.ru',
                'USER_ID' => '606'
            ),
            1 => array(
                'NAME' => 'bells',
                'EMAIL' => 'bells@salatniza.ru',
                'USER_ID' => '11'
            )
        );
    }

    public function getFetchDataModifier($fields)
    {
        if(isset($fields['FUSER_USER_NAME']))
        {
            $fields['NAME'] = $fields['FUSER_USER_NAME'];
            unset($fields['FUSER_USER_NAME']);
        }

        return $fields;
    }

    public function getPersonalizeFields()
    {
        if($this->isRunForOldData()) {
            $oldData = "Y";
        }else{
            $oldData = "N";
        }

        return array(
            'PERIOD' => $this->getFieldValue('MINUTES_BASKET_FORGOTTEN'),
            'OLD_DATA' => $oldData
        );
    }

    public static function getPersonalizeList()
    {
        return array(
            array(
                'CODE' => 'PERIOD',
                'NAME' => 'За какое время считать',
                'DESC' => 'С какой даты до момента запуска триггера ищутся брошенные корзины'
            ),
            array(
                'CODE' => 'OLD_DATA',
                'NAME' => 'Считать за все время',
                'DESC' => 'Считать брошенные корзины за все время до момента запуска триггера'
            ),
        );
    }
}