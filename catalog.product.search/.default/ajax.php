<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>
<?
//обработчик для сокращения числа выбираемых брендов - Ямангулов.
$brands = array();
if($_REQUEST['id'] != 0){
    $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 2, "SECTION_ID" => $_REQUEST['id'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID", "IBLOCK_ID", "PROPERTY_BRAND"));
}else{
    $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 2, "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID", "IBLOCK_ID", "PROPERTY_BRAND"));
}

while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $brands[] = $arFields["PROPERTY_BRAND_VALUE"];
}
$brands = array_values(array_unique($brands));
if(count($brands) != 0){
    print_r($brands);
}