<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

\CJSCore::Init( 'jquery' );
\CJSCore::RegisterExt("samizoo_ext", Array(
    "js" => "/static/js/external.js",
    "css" => "/static/css/external.css",
    "skip_core" => true,
));
\CJSCore::Init(array("samizoo_ext")); ?>
<script>
    $(document).ready(function () {
        $('input[name="find_PROP_PHONE"]').inputmask('+7(999) 999-99-99', {
            showMaskOnFocus: true
        });
        $('input[name="find_PROP_MOBILE_PHONE"]').inputmask('+7(999) 999-99-99', {
            showMaskOnFocus: true
        });
    });
</script>
