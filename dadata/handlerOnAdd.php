<?php
//print_r($_REQUEST);
//writeToLog($_REQUEST, 'incoming by adding deal');

/**
 * Write data to log file.
 *
 * @param mixed $data
 * @param string $title
 *
 * @return bool
 */
function writeToLog($data, $title = '') {
    $log = "\n------------------------\n";
    $log .= date("Y.m.d G:i:s") . "\n";
    $log .= (strlen($title) > 0 ? $title : 'DEBUG') . "\n";
    $log .= print_r($data, 1);
    $log .= "\n------------------------\n";
    file_put_contents(__DIR__ . '/hook.log', $log, FILE_APPEND);
    return true;
}

//writeToLog($_REQUEST, 'request');

//получение поля ddata новой сделки
$queryUrl = 'https://'.$_REQUEST['auth']['domain'].'/rest/crm.deal.list.json?auth='.$_REQUEST["auth"]["access_token"];
$queryData = http_build_query(array(
    'filter' => array(
        "ID" => $_REQUEST["data"]["FIELDS"]["ID"],
    ),
    'select' => array(
        "UF_CRM_DDATA_ADDRESS"
    ),
));
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));

$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result, 1);
//writeToLog($result, 'ddata field of new deal');

//раскидываем информацию в новой сделке по дополнительным полям
$arFields = $result["result"][0]["UF_CRM_DDATA_ADDRESS"];
//writeToLog($arFields, 'array ddata field of new deal');

$metro = ($arFields[4] != 'null' && $arFields[4] != null)?($arFields[4]):'';
$queryUrl = 'https://'.$_REQUEST['auth']['domain'].'/rest/crm.deal.update.json?auth='.$_REQUEST["auth"]["access_token"];
$queryData = http_build_query(array(
    'id' => $_REQUEST["data"]["FIELDS"]["ID"],
    'fields' => array(
        "UF_CRM_ADDRESS" => $arFields[0],
        "UF_CRM_REGION" => $arFields[1],
        "UF_CRM_CITY" => $arFields[2],
        "UF_CRM_STREET" => $arFields[3],
        "UF_CRM_UNDERGROUND" => $metro,
        "UF_CRM_LAT" => $arFields[5],
        "UF_CRM_LON" => $arFields[6],
    ),
    'params' => array("REGISTER_SONET_EVENT" => "N")
));

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));

$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result, 1);
writeToLog($result, 'updating new deal succeed');

