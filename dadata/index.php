<?php
//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
$placement = $_REQUEST['PLACEMENT'];
$placementOptions = isset($_REQUEST['PLACEMENT_OPTIONS']) ? json_decode($_REQUEST['PLACEMENT_OPTIONS'], true) : array();
$handler = ($_SERVER['SERVER_PORT'] === '443' ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];
$handlerOnAdd = ($_SERVER['SERVER_PORT'] === '443' ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].'/embed/handlerOnAdd.php';
$handlerKey = ($_SERVER['SERVER_PORT'] === '443' ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].'/embed/handlerKey.php';


if(!is_array($placementOptions))
{
	$placementOptions = array();
}

if($placement === 'DEFAULT')
{
	$placementOptions['MODE'] = 'edit';
}
?>
<!DOCTYPE html>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.6.0/dist/css/suggestions.min.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!--[if lt IE 10]>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.6.0/dist/js/jquery.suggestions.min.js"></script>
	<script src="//api.bitrix24.com/api/v1/dev/"></script>
    <style>
        .input_frame{
            height: 25px;
            padding: 0 2px;
            vertical-align: top;
            width: 250px!important;
            border: 1px solid #d9d9d9;
        }
        .mfields {
            list-style: none;
            -webkit-margin-before: 0;
            -webkit-margin-after: 0;
        }
        .suggestions-addon {
            display: none!important;
            width:0!important;
            height:0!important;
        }
    </style>
</head>
<body style="margin: 0; padding: 0; background-color: <?php echo $placementOptions['MODE'] === 'edit' ? '#fff' : 'white'?>;">
<div class="workarea">
<?php
if($placement === 'DEFAULT'):
?>
    <script>

        var test = {
            call: function(method, param)
            {
                BX24.callMethod(method, param, test.debug);
            },

            debug: function(result)
            {
                var s = '';

                s += '<b>' + result.query.method + '</b>\n';
                s += JSON.stringify(result.query.data, null, '  ') + '\n\n';

                if(result.error())
                {
                    s += '<span style="color: red">Error! ' + result.error().getStatus() + ': ' + result.error().toString() + '</span>\n';
                }
                else
                {
                    s += '<span>' + JSON.stringify(result.data(), null, '  ') + '</span>\n';
                }

                document.getElementById('debug').innerHTML = s;
            },

            add: function()
            {
                test.call('userfieldtype.add', {
                    USER_TYPE_ID: 'ddata_address',
                    HANDLER: '<?php echo $handler?>',
                    TITLE: 'Ddata autofilled type',
                    DESCRIPTION: 'Field with autofilling from ddata'
                });
            },

            addField: function()
            {
                test.call(
                    "crm.deal.userfield.add",
                    {
                        fields:
                            {
                                "FIELD_NAME": "ddata_address",
                                "EDIT_FORM_LABEL": "Местоположение",
                                "LIST_COLUMN_LABEL": "Местоположение",
                                "USER_TYPE_ID": "ddata_address",
                                "XML_ID": "DDATA_ADDRESS",
                                "MULTIPLE": "Y",
                                "SETTINGS": {}
                            }
                    },
                    function(result)
                    {
                        if(result.error())
                            console.error(result.error());
                        else
                            console.dir(result.data());
                    }
                );
            },

            addFields: function()
            {
                test.call(
                    "crm.deal.userfield.add",
                    {
                        fields:
                            {
                                "FIELD_NAME": "address",
                                "EDIT_FORM_LABEL": "Адрес",
                                "LIST_COLUMN_LABEL": "Адрес",
                                "USER_TYPE_ID": "string",
                                "XML_ID": "ADDRESS",
                                "MULTIPLE": "N",
                                "SETTINGS": {}
                            }
                    },
                    function(result)
                    {
                        if(result.error())
                            console.error(result.error());
                        else
                            console.dir(result.data());
                    }
                );
                test.call(
                    "crm.deal.userfield.add",
                    {
                        fields:
                            {
                                "FIELD_NAME": "region",
                                "EDIT_FORM_LABEL": "Регион",
                                "LIST_COLUMN_LABEL": "Регион",
                                "USER_TYPE_ID": "string",
                                "XML_ID": "REGION",
                                "MULTIPLE": "N",
                                "SETTINGS": {}
                            }
                    },
                    function(result)
                    {
                        if(result.error())
                            console.error(result.error());
                        else
                            console.dir(result.data());
                    }
                );
                test.call(
                    "crm.deal.userfield.add",
                    {
                        fields:
                            {
                                "FIELD_NAME": "city",
                                "EDIT_FORM_LABEL": "Город",
                                "LIST_COLUMN_LABEL": "Город",
                                "USER_TYPE_ID": "string",
                                "XML_ID": "CITY",
                                "MULTIPLE": "N",
                                "SETTINGS": {}
                            }
                    },
                    function(result)
                    {
                        if(result.error())
                            console.error(result.error());
                        else
                            console.dir(result.data());
                    }
                );
                test.call(
                    "crm.deal.userfield.add",
                    {
                        fields:
                            {
                                "FIELD_NAME": "street",
                                "EDIT_FORM_LABEL": "Улица",
                                "LIST_COLUMN_LABEL": "Улица",
                                "USER_TYPE_ID": "string",
                                "XML_ID": "STREET",
                                "MULTIPLE": "N",
                                "SETTINGS": {}
                            }
                    },
                    function(result)
                    {
                        if(result.error())
                            console.error(result.error());
                        else
                            console.dir(result.data());
                    }
                );
                test.call(
                    "crm.deal.userfield.add",
                    {
                        fields:
                            {
                                "FIELD_NAME": "underground",
                                "EDIT_FORM_LABEL": "Ближайшее метро",
                                "LIST_COLUMN_LABEL": "Ближайшее метро",
                                "USER_TYPE_ID": "string",
                                "XML_ID": "UNDERGROUND",
                                "MULTIPLE": "N",
                                "SETTINGS": {}
                            }
                    },
                    function(result)
                    {
                        if(result.error())
                            console.error(result.error());
                        else
                            console.dir(result.data());
                    }
                );
                test.call(
                    "crm.deal.userfield.add",
                    {
                        fields:
                            {
                                "FIELD_NAME": "lat",
                                "EDIT_FORM_LABEL": "Широта",
                                "LIST_COLUMN_LABEL": "Широта",
                                "USER_TYPE_ID": "string",
                                "XML_ID": "LAT",
                                "MULTIPLE": "N",
                                "SETTINGS": {}
                            }
                    },
                    function(result)
                    {
                        if(result.error())
                            console.error(result.error());
                        else
                            console.dir(result.data());
                    }
                );
                test.call(
                    "crm.deal.userfield.add",
                    {
                        fields:
                            {
                                "FIELD_NAME": "lon",
                                "EDIT_FORM_LABEL": "Долгота",
                                "LIST_COLUMN_LABEL": "Долгота",
                                "USER_TYPE_ID": "string",
                                "XML_ID": "LON",
                                "MULTIPLE": "N",
                                "SETTINGS": {}
                            }
                    },
                    function(result)
                    {
                        if(result.error())
                            console.error(result.error());
                        else
                            console.dir(result.data());
                    }
                );
            },

            list: function()
            {
                test.call('userfieldtype.list', {});
            },

            update: function()
            {
                test.call('userfieldtype.update', {
                    USER_TYPE_ID: 'ddata_address',
                    DESCRIPTION: 'Field with autofilling from ddata ' + (new Date()).toString()
                });
            },

            del: function()
            {
                test.call('userfieldtype.delete', {
                    USER_TYPE_ID: 'ddata_address'
                });
            },
            
            addOnCrmDealAddHandler: function()
            {
                test.call('event.bind', {
                    event: 'onCrmDealAdd',
                    handler: '<?php echo $handlerOnAdd?>'
                });
            },

            delOnCrmDealAddHandler: function()
            {
                test.call('event.unbind', {
                    event: 'onCrmDealAdd',
                    handler: '<?php echo $handlerOnAdd?>'
                });
            },

            handlersGet: function()
            {
                test.call('event.get');
            },
        }

        $("#apikey").submit(function(e) {

            e.preventDefault();
            var url = "<?php echo $handlerKey?>";

            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                success: function(data)
                {
                    $('#response').text("Ваш ключ добавлен в хранилище");
                }
            });
        });

    </script>
    <form action="<?php echo $handlerKey?>" method="post" id="apikey">
        <label>Введите Ваш API-ключ сервиса dadata</label>
        <input type="hidden" name="auth" value='<?php echo $_REQUEST["AUTH_ID"]?>'>
        <input type="text" name="dadataTicket" size="50">
        <input type="submit"><br>
        <span id="response"></span>
    </form>
	<ul>
		<li><a href="javascript:void(0)" onclick="test.add()">Добавить тип поля ddata_address</a></li>
		<li><a href="javascript:void(0)" onclick="test.addField()">Добавить к сделке поле ddata_address для автозаполнения</a></li>
		<li><a href="javascript:void(0)" onclick="test.addFields()">Добавить к сделке дополнительные поля</a></li>
		<li><a href="javascript:void(0)" onclick="test.list()">Показать кастомные типы полей</a></li>
<!--		<li><a href="javascript:void(0)" onclick="test.update()">Обновить тип поля ddata_address</a></li>-->
		<li><a href="javascript:void(0)" onclick="test.del()">Удалить тип поля ddata_address</a></li>
		<li><a href="javascript:void(0)" onclick="test.addOnCrmDealAddHandler()">Добавить обработчик при добавлении новой сделки</a></li>
		<li><a href="javascript:void(0)" onclick="test.handlersGet()">Показать кастомные обработчики событий</a></li>
		<li><a href="javascript:void(0)" onclick="test.delOnCrmDealAddHandler()">Удалить обработчик при добавлении новой сделки</a></li>
	</ul>
	<pre id="debug" style="border: solid 1px #aaa; padding: 10px; background-color: #eee">&nbsp;</pre>

<?php
elseif($placement === 'USERFIELD_TYPE'):
	if($placementOptions['MODE'] === 'edit')
	{
		if($placementOptions['MULTIPLE'] === 'N')
		{
?>
	<input type="text" class="input_frame" style="width: 90%;" value="<?php echo htmlspecialchars($placementOptions['VALUE'])?>" onkeyup="setValue(this.value)">
    <script>
		function setValue(value)
		{
			BX24.placement.call('setValue', value);
		}
	</script>
<?php
		}
		else
		{
?>
	<textarea id="ddata" style="width: 97%; height: 130px;"><?php echo htmlspecialchars(implode("\n", $placementOptions['VALUE']))?></textarea>

<?php
		}
	}
	else
	{

		if(is_array($placementOptions['VALUE']))
		{?>
            <ul class="mfields">
                <?php
            $arFieldsNames = array(
                "Адрес",
                "Регион",
                "Город",
                "Улица",
                "Ближайшее метро",
                "Широта",
                "Долгота"
            );
            ?>
                <?php foreach($placementOptions['VALUE'] as $key => $value)
			{
			    if($value != null && $value != 'null'){
                    echo '<li>'.$arFieldsNames[$key].': '.htmlspecialchars($value).'</li>';
                }
			}?>
            </ul>
<?php }
		else
		{
			echo '<i>'.htmlspecialchars($placementOptions['VALUE']).'</i>';
		}

	}

endif;
?>
</div>
<?php
$queryUrl = 'https://'.$_REQUEST['DOMAIN'].'/rest/entity.item.get.json?auth='.$_REQUEST["AUTH_ID"];
$queryData = http_build_query(array(
    'ENTITY' => 'ddatakeys',
    'FILTER' => array(
        'NAME' => 'ddatakey',
    ),
));
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));
$result = curl_exec($curl);
curl_close($curl);
$result = json_decode($result, 1);
$dadataToken = $result['result'][0]['PREVIEW_TEXT'];
//echo $dadataToken;
?>
<script>
	BX24.ready(function()
	{
        function callbackUpdate(id, suggestion ) {
            //console.log(id);
            BX24.callMethod(
                "crm.deal.update",
                {
                    id: id,
                    fields:
                        {
                            "UF_CRM_ADDRESS":     suggestion.value,
                            "UF_CRM_REGION":      suggestion.data.region_with_type,
                            "UF_CRM_CITY":        suggestion.data.city,
                            "UF_CRM_STREET":      suggestion.data.street_with_type,
                            "UF_CRM_UNDERGROUND": metro,
                            "UF_CRM_LAT":         suggestion.data.geo_lat,
                            "UF_CRM_LON":         suggestion.data.geo_lon,
                            "UF_CRM_DDATA_ADDRESS": $("#ddata").val().split('\n')
                        },
                    params: { "REGISTER_SONET_EVENT": "N" }
                },
                function(result)
                {
                    if(result.error())
                        console.error(result.error());
                    else
                    {
                        console.info(result.data());
                    }
                }
            );
        }

        function callbackEmpty(id) {
            //console.log(id);
            BX24.callMethod(
                "crm.deal.update",
                {
                    id: id,
                    fields:
                        {
                            "UF_CRM_ADDRESS":     '',
                            "UF_CRM_REGION":      '',
                            "UF_CRM_CITY":        '',
                            "UF_CRM_STREET":      '',
                            "UF_CRM_UNDERGROUND": '',
                            "UF_CRM_LAT":         '',
                            "UF_CRM_LON":         '',
                            "UF_CRM_DDATA_ADDRESS": $("#ddata").val().split('\n')
                        },
                    params: { "REGISTER_SONET_EVENT": "N" }
                },
                function(result)
                {
                    if(result.error())
                        console.error(result.error());
                    else
                    {
                        console.info(result.data());
                    }
                }
            );
        }


	    var placementOptions = <?php echo ($_REQUEST['PLACEMENT_OPTIONS'])?($_REQUEST['PLACEMENT_OPTIONS']):'null'?>;
	    if(placementOptions != 'null' && placementOptions != null)
	        var id = placementOptions.ENTITY_VALUE_ID;
	    //console.log(id);

        BX24.init(function()
		{
			BX24.resizeWindow(document.getElementsByClassName("workarea")[0].clientWidth,
				document.getElementsByClassName("workarea")[0].clientHeight * 2);
		});


        //автозаполнение
        var metro;
        metro = '';
        $("#ddata").suggestions({
            token: '<?php echo $dadataToken?>',
            type: "ADDRESS",
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                if(suggestion.data['metro'] != 'null' && suggestion.data['metro'] != null) {
                    suggestion.data['metro'].forEach(function(item, i, suggestion){
                        metro += 'Линия: ' + item.line + ', станция: ' + item.name + ', расстояние: ' + item.distance + ' км' + ' | ';
                    });
                }

                var newSuggestion = suggestion.value + '\n' + suggestion.data.region_with_type + '\n' + suggestion.data.city + '\n' + suggestion.data.street_with_type + '\n' + metro + '\n' + suggestion.data.geo_lat + '\n' + suggestion.data.geo_lon;

//                console.log(metro);
//                console.log(suggestion.data['metro']);

                $("#ddata").val(newSuggestion);
                if(id != 0){
                    BX24.placement.call('setValue', $("#ddata").val().split('\n'), callbackUpdate(id, suggestion));
                }else{
                    BX24.placement.call('setValue', $("#ddata").val().split('\n'));
                }
            },
	    });

        //очистка местоположения
        if(id != 0){
            $("#ddata").on('input', function () {
                BX24.placement.call('setValue', $("#ddata").val().split('\n'));
                BX24.placement.call('setValue', $("#ddata").val().split('\n'), callbackEmpty(id));
            });
        }

	});
</script>

</body>
</html>
