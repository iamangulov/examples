<?php
//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
function writeToLog($data, $title = '') {
    $log = "\n------------------------\n";
    $log .= date("Y.m.d G:i:s") . "\n";
    $log .= (strlen($title) > 0 ? $title : 'DEBUG') . "\n";
    $log .= print_r($data, 1);
    $log .= "\n------------------------\n";
    file_put_contents(__DIR__ . '/hook.log', $log, FILE_APPEND);
    return true;
}

//получение текущего пользователя, под которым ставится приложение - следовательно, он уже имеет права администратора
$queryUrl = 'https://'.$_REQUEST['auth']['domain'].'/rest/user.current.json?auth='.$_REQUEST["auth"];
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));
$result = curl_exec($curl);
curl_close($curl);
$result = json_decode($result, 1);
$user = $result['result']['ID'];

//создание хранилища для api ключа сервиса dadata
$queryUrl = 'https://'.$_REQUEST['auth']['domain'].'/rest/entity.add.json?auth='.$_REQUEST["auth"];
$queryData = http_build_query(array(
    'ENTITY' => 'ddatakeys',
    'NAME' => 'ddatakeys',
    'ACCESS' => array(
        'U'.$user => 'W',
        'U1' => 'W',
        'AU' => 'R'
    )
));
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));
curl_exec($curl);
curl_close($curl);

//проверка, есть ли записи в хранилище, и если есть - то очистка, а затем запись новой
$queryUrl = 'https://'.$_REQUEST['auth']['domain'].'/rest/entity.item.get.json?auth='.$_REQUEST["auth"];
$queryData = http_build_query(array(
    'ENTITY' => 'ddatakeys',
    'FILTER' => array(
        'NAME' => 'ddatakey',
    ),
));
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));
$result = curl_exec($curl);
curl_close($curl);
$result = json_decode($result, 1);

if(count($result['result']) > 0)
{
    foreach ($result['result'] as $element)
    {
        $queryUrl = 'https://'.$_REQUEST['auth']['domain'].'/rest/entity.item.delete.json?auth='.$_REQUEST["auth"];
        $queryData = http_build_query(array(
            'ENTITY' => 'ddatakeys',
            'ID' => $element['ID'],
        ));
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));
        curl_exec($curl);
        curl_close($curl);
    }
}

//запись в хранилище ключа
$queryUrl = 'https://'.$_REQUEST['auth']['domain'].'/rest/entity.item.add.json?auth='.$_REQUEST["auth"];
$queryData = http_build_query(array(
    'ENTITY' => 'ddatakeys',
    'NAME' => 'ddatakey',
    'PREVIEW_TEXT' => $_REQUEST["dadataTicket"]
));
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));
$result = curl_exec($curl);
curl_close($curl);
if($result)
    echo "Ваш ключ добавлен/обновлен! Перезагрузите страницу приложения!";
