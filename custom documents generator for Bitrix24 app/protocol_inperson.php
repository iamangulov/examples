<?php
@set_time_limit(0);
@ignore_user_abort(true);

require_once './vendor/autoload.php';
use \PhpOffice\PhpWord\Shared\Converter;

//функция для записи в лог
function writeToLog($data, $title = ''){
    $log = "\n------------------------\n";
    $log .= date("Y.m.d G:i:s") . "\n";
    $log .= (strlen($title) > 0 ? $title : 'DEBUG') . "\n";
    $log .= print_r($data, 1);
    $log .= "\n------------------------\n";
    file_put_contents(__DIR__ . '/vnii.log', $log, FILE_APPEND);
    return true;
}
//writeToLog($_REQUEST, 'request');
//получим дату начала обучения и состав слушателей
$id = substr($_REQUEST["document_id"][2], 5);
$queryUrl = 'https://'.$_REQUEST['auth']['domain'].'/rest/18/'.$_REQUEST['auth_string'].'/crm.deal.list.json';
$queryData = http_build_query(array(
    'filter' => array(
        "ID" => $id
    ),
    'select' => array(
        "UF_CRM_5B96705235DB6",//дата окончания обучения
        "UF_CRM_5B967051E76EB",//фио слушателей
        "UF_CRM_5B967051F1482",//должность слушателей
        "UF_CRM_1542972642",    //номер протокола
        "UF_CRM_1543225842",    //председатель комисси
        "UF_CRM_1543226133",    //зам. преседателя
        "UF_CRM_1543226343",    //члены комиссии
    ),
));
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));

$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result, 1);
//writeToLog($result, 'fields from new deal');

$chairmanId = $result['result'][0]['UF_CRM_1543225842'];
$deputyId = $result['result'][0]['UF_CRM_1543226133'];
$membersIds = $result['result'][0]['UF_CRM_1543226343'];

$dateFull = substr($result['result'][0]['UF_CRM_5B96705235DB6'], 0, 10);
if($result['result'][0]['UF_CRM_1542972642'])
    $protocol = $result['result'][0]['UF_CRM_1542972642'];
else
    $protocol = "б/н";
$fios = $result['result'][0]['UF_CRM_5B967051E76EB'];
$positions = $result['result'][0]['UF_CRM_5B967051F1482'];
$corr = [
    1 => 'января',
    2 => 'февраля',
    3 => 'марта',
    4 => 'апреля',
    5 => 'мая',
    6 => 'июня',
    7 => 'июля',
    8 => 'августа',
    9 => 'сентября',
    10 => 'октября',
    11 => 'ноября',
    12 => 'декабря'
];
$date = substr($dateFull, 8, 2);
$month = $corr[substr($dateFull, 5, 2)];
$year = substr($dateFull, 0, 4);

//получения описания полей сделки
$queryUrl = 'https://'.$_REQUEST['auth']['domain'].'/rest/18/'.$_REQUEST['auth_string'].'/crm.deal.fields.json';

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));

$fields = curl_exec($curl);
curl_close($curl);

$fields = json_decode($fields, 1);
//writeToLog($fields, 'description fields for deal');
//получение значений полей типа список для состава комиссии
foreach($fields['result']['UF_CRM_1543225842']['items'] as $value){
    if($value['ID'] == $chairmanId){
        $chairman = $value['VALUE'];
    }
}
foreach($fields['result']['UF_CRM_1543226133']['items'] as $value){
    if($value['ID'] == $deputyId){
        $deputy = $value['VALUE'];
    }
}
$members = [];
foreach ($membersIds as $membersId){
    foreach($fields['result']['UF_CRM_1543226343']['items'] as $value){
        if($value['ID'] == $membersId){
            $members[] = $value['VALUE'];
        }
    }
}

$word = new \PhpOffice\PhpWord\PhpWord();

$word->setDefaultFontName('DejaVuSerif');
$word->setDefaultFontSize(12);

$meta = $word->getDocInfo();
$meta->setCreator('');
$meta->setCompany('');
$meta->setTitle('');
$meta->setDescription('');
$meta->setCategory('');
$meta->setLastModifiedBy('');
$meta->setCreated( date('m-d-Y', time()) ); // Дата и время создания документа
$meta->setModified( date('m-d-Y', time()) ); //Дата и время последнего изменения документа
$meta->setSubject('');
$meta->setKeywords('');

$sectionStyle = array('orientation' => 'landscape',
    'marginLeft' => Converter::pixelToTwip(80),
    'marginRight' => Converter::pixelToTwip(40),
    'marginTop' => Converter::pixelToTwip(60),
    'borderColor' => 'none',
    'borderSize' => 'none'
);
//первая страница
$section = $word->addSection($sectionStyle);

$text = 'ФЕДЕРАЛЬНОЕ ГОСУДАРСТВЕННОЕ БЮДЖЕТНОЕ УЧРЕЖДЕНИЕ';
$fontStyle = array('name'=>'DejaVuSerif', 'size'=>11, 'color'=>'000000', 'bold'=>TRUE, 'italic'=>FALSE);
$parStyle = array('align'=>'center','spaceBefore'=>0);
$section->addText(htmlspecialchars($text), $fontStyle, $parStyle);

$text = '«ВСЕРОССИЙСКИЙ НАУЧНО-ИССЛЕДОВАТЕЛЬСКИЙ ИНСТИТУТ ТРУДА»';
$fontStyle = array('name'=>'DejaVuSerif', 'size'=>11, 'color'=>'000000', 'bold'=>TRUE, 'italic'=>FALSE);
$parStyle = array('align'=>'center','spaceBefore'=>0);
$section->addText(htmlspecialchars($text), $fontStyle, $parStyle);

$text = 'МИНИСТЕРСТВА ТРУДА И СОЦИАЛЬНОЙ ЗАЩИТЫ РОССИЙСКОЙ ФЕДЕРАЦИИ';
$fontStyle = array('name'=>'DejaVuSerif', 'size'=>11, 'color'=>'000000', 'bold'=>TRUE, 'italic'=>FALSE);
$parStyle = array('align'=>'center','spaceBefore'=>0);
$section->addText(htmlspecialchars($text), $fontStyle, $parStyle);
$section->addTextBreak();

$text = $date.' '.$month.' '.$year.' г.';
$fontStyle = array('name'=>'DejaVuSerif', 'size'=>11, 'color'=>'000000', 'bold'=>TRUE, 'italic'=>FALSE);
$parStyle = array('align'=>'right','spaceBefore'=>0);
$section->addText(htmlspecialchars($text), $fontStyle, $parStyle);
$section->addTextBreak();

$text = 'ПРОТОКОЛ № '.$protocol;
$fontStyle = array('name'=>'DejaVuSerif', 'size'=>11, 'color'=>'000000', 'bold'=>TRUE, 'italic'=>FALSE);
$parStyle = array('align'=>'center','spaceBefore'=>0);
$section->addText(htmlspecialchars($text), $fontStyle, $parStyle);

$text = 'заседания комиссии по проверке знаний требований охраны труда руководителей и специалистов,  членов комиссий по проверке знаний требований охраны труда  организаций';
$fontStyle = array('name'=>'DejaVuSerif', 'size'=>11, 'color'=>'000000', 'bold'=>TRUE, 'italic'=>FALSE);
$parStyle = array('align'=>'center','spaceBefore'=>0);
$section->addText(htmlspecialchars($text), $fontStyle, $parStyle);
$section->addTextBreak(2);

$text = 'В соответствии с приказом (распоряжением) работодателя (руководителя) организации  от  "___" ________ 20__ г. № ____ комиссия в составе:';
$fontStyle = array('name'=>'DejaVuSerif', 'size'=>12, 'color'=>'000000', 'bold'=>FALSE, 'italic'=>FALSE);
$parStyle = array('align'=>'center','spaceBefore'=>0);
$section->addText(htmlspecialchars($text), $fontStyle, $parStyle);
$section->addTextBreak(2);

$tableStyle = array(
    'borderColor' => 'ffffff',
    'borderSize'  => Converter::pixelToTwip(0),
    'alignment' => 'center'
);
$table = $section->addTable($tableStyle);

$table->addRow();
$fontStyle = array('size' => 12);
$parStyle = array('align' => 'left');
$arChairman = explode(' - ', $chairman);
$table->addCell(Converter::pixelToTwip(150))->addText("председателя:", $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(45))->addText("-", $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(150))->addText($arChairman[0], $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(500))->addText($arChairman[1], $fontStyle, $parStyle);

$table->addRow();
$fontStyle = array('size' => 12);
$parStyle = array('align' => 'left');
$arDeputy = explode(' - ', $deputy);
$table->addCell(Converter::pixelToTwip(150))->addText("зам. председателя:", $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(45))->addText("-", $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(150))->addText($arDeputy[0], $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(500))->addText($arDeputy[1], $fontStyle, $parStyle);

foreach ($members as $key => $member){
    $table->addRow();
    $fontStyle = array('size' => 12);
    $parStyle = array('align' => 'left');
    $arMember = explode(' - ', $member);
    if($key == 0){
        $table->addCell(Converter::pixelToTwip(150))->addText("членов:", $fontStyle, $parStyle);
    }else{
        $table->addCell(Converter::pixelToTwip(150))->addText(" ", $fontStyle, $parStyle);

    }
    $table->addCell(Converter::pixelToTwip(45))->addText("-", $fontStyle, $parStyle);
    $table->addCell(Converter::pixelToTwip(150))->addText($arMember[0], $fontStyle, $parStyle);
    $table->addCell(Converter::pixelToTwip(500))->addText($arMember[1], $fontStyle, $parStyle);
}
$section->addTextBreak(4);

$text = 'провела проверку знаний  требований охраны труда по программе повышения квалификации руководителей и специалистов «Безопасность и охрана труда»  в объеме 72 часов';
$fontStyle = array('name'=>'DejaVuSerif', 'size'=>11, 'color'=>'000000', 'bold'=>FALSE, 'italic'=>FALSE);
$parStyle = array('align'=>'left','spaceBefore'=>0);
$section->addText(htmlspecialchars($text), $fontStyle, $parStyle);
$section->addTextBreak(2);

$sectionStyle = array('orientation' => 'landscape',
    'marginLeft' => Converter::pixelToTwip(80),
    'marginRight' => Converter::pixelToTwip(40),
    'marginTop' => Converter::pixelToTwip(60),
    'borderColor' => 'none',
    'borderSize' => 'none'
);
//вторая страница
$section = $word->addSection($sectionStyle);

$tableStyle = array(
    'borderColor' => '000000',
    'borderSize'  => Converter::pixelToTwip(0.5),
    'alignment' => 'center'
);
$table = $section->addTable($tableStyle);

$table->addRow();
$fontStyle = array('size' => 11, 'bold'=>FALSE);
$parStyle = array('align' => 'center');
$table->addCell(Converter::pixelToTwip(45))->addText('№ пп', $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(200))->addText('Фамилия, имя, отчество', $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(250))->addText('Должность', $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(150))->addText('Наименование  подразделения (цех, участок, отдел, лаборатория и т.д.)', $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(80))->addText('Результат проверки знаний (сдал/не сдал) № выданного удостоверения', $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(150))->addText('Причина проверки знаний (очередная, внеочередная и т.д.)', $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(150))->addText('Подпись проверяемого', $fontStyle, $parStyle);

foreach ($fios as $key => $fio){
    $table->addRow();
    $fontStyle = array('size' => 11);
    $parStyle = array('align' => 'left');
    $table->addCell(Converter::pixelToTwip(45))->addText($key + 1, $fontStyle, $parStyle);
    $table->addCell(Converter::pixelToTwip(200))->addText($fios[$key], $fontStyle, $parStyle);
    $table->addCell(Converter::pixelToTwip(250))->addText($positions[$key], $fontStyle, $parStyle);
    $table->addCell(Converter::pixelToTwip(150))->addText('', $fontStyle, $parStyle);
    $table->addCell(Converter::pixelToTwip(80))->addText('', $fontStyle, $parStyle);
    $table->addCell(Converter::pixelToTwip(150))->addText('', $fontStyle, $parStyle);
    $table->addCell(Converter::pixelToTwip(150))->addText('', $fontStyle, $parStyle);
}
$section->addTextBreak(2);

$tableStyle = array(
    'borderColor' => 'ffffff',
    'borderSize'  => Converter::pixelToTwip(0),
    'alignment' => 'left',
    'cellMarginBottom' => Converter::pixelToTwip(15)
);
$table = $section->addTable($tableStyle);
$table->addRow();
$fontStyle = array('size' => 12);
$parStyle = array('align' => 'left');
$arChairman = explode(' - ', $chairman);
$table->addCell(Converter::pixelToTwip(200))->addText("Председатель комиссии:", $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(300))->addText("___________________________________", $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(200))->addText($arChairman[2], $fontStyle, $parStyle);

$table->addRow();
$fontStyle = array('size' => 12);
$parStyle = array('align' => 'left');
$arDeputy = explode(' - ', $deputy);
$table->addCell(Converter::pixelToTwip(200))->addText("Зам. председателя:", $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(300))->addText("___________________________________", $fontStyle, $parStyle);
$table->addCell(Converter::pixelToTwip(200))->addText($arDeputy[2], $fontStyle, $parStyle);

foreach ($members as $key => $member){
    $table->addRow();
    $fontStyle = array('size' => 12);
    $parStyle = array('align' => 'left');
    $arMember = explode(' - ', $member);
    if($key == 0){
        $table->addCell(Converter::pixelToTwip(200))->addText("Члены комиссии:", $fontStyle, $parStyle);
    }else{
        $table->addCell(Converter::pixelToTwip(200))->addText(" ", $fontStyle, $parStyle);

    }
    $table->addCell(Converter::pixelToTwip(300))->addText("___________________________________", $fontStyle, $parStyle);
    $table->addCell(Converter::pixelToTwip(200))->addText($arMember[2], $fontStyle, $parStyle);
}

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($word, 'Word2007');
$filename = './docs/protocol_inperson_'.$id.'.docx';
$objWriter->save($filename);

$file = base64_encode(file_get_contents($filename));

$queryUrl = 'https://'.$_REQUEST['auth']['domain'].'/rest/18/'.$_REQUEST['auth_string'].'/crm.deal.update.json';
$queryData = http_build_query(array(
    'id' => $id,
    'fields' => array(
        "UF_CRM_1543472299" => ['fileData' => ['protocol_inperson_'.$id.'.docx', $file]],
    ),
));

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));

$result = curl_exec($curl);
curl_close($curl);

unlink($filename);
