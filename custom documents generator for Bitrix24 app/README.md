Приложение (веб-хук для подключения в бизнес-процессах Битрикс 24). Генерирует документ из полей сделки, умеет обрабатывать множественные поля сделки и заносить данные из них в таблицу, что не умеют делать стандарные средства для генерации документов Битрикс24.

Application (web hook for connecting to business process Bitrix 24). Generates a document from the deal fields, can process multiple deal fields and enter data from them into a table that they cannot make standard means for generating Bitrix24  documents.
