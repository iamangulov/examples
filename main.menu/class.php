<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


class SamizooMenuComponent extends CBitrixComponent
{
    public function executeComponent()
    {

        if (!CModule::IncludeModule("sale")) return;

        // кэшируем меню на день
        $cntIBLOCK_List = 'catalog';
        $cache = new CPHPCache();
        $cache_time = 86400;
        $cache_id = 'menuResult' . $cntIBLOCK_List;
        $cache_path = 'menuResult';

        if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path)) {
            $res = $cache->GetVars();
            if (is_array($res["menuResult"]["CATALOG"]) && count($res["menuResult"]["CATALOG"]) > 0) {
                $this->arResult = $res["menuResult"];
            }
        }

        if (!is_array($this->arResult["CATALOG"])) {
            $arCatalogGroup = [];
            $allCatalog = [];

            //отдельно хранящиеся группы продуктов
            $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
            $arFilter = Array("IBLOCK_CODE" => 'products_group', "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", );
            //echo microtime()."--1--";
            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
            $res = CIBlockElement::GetList(array("SORT" => "asc"), $arFilter, false, false, $arSelect);
            while ($ob = $res->GetNext()) {
                $arCatalogGroup["CATALOG_GROUP"][$ob["ID"]]["NAME"] = $ob["NAME"];
            }
            //echo microtime()."--2--";
            //узнаем id
            $res = CIBlock::GetList(Array(), Array("CODE" => 'catalog'), true);
            while ($ar_res = $res->Fetch()) {
                $idCatalog = $ar_res['ID'];
            }
            //echo microtime()."--3--";
            //выбираем все акционные товары
            $sharedAction = $this->get_shared_action();
            //echo microtime()."--4--";
            //выбираем все акционные товары
            $level3WithBrand = $this->get_brands();
            //echo microtime()."--5--";
            //выбираем все разделы
            $arFilter = array('IBLOCK_ID' => $idCatalog, '>=DEPTH_LEVEL' => 1, 'ACTIVE' => 'Y');
            $rsSect = CIBlockSection::GetList(
                ['left_margin' => 'asc'],
                $arFilter,
                true,
                ["ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "CODE", "NAME", "SECTION_PAGE_URL", "UF_GROUP", "DEPTH_LEVEL", "PICTURE", "UF_PICTURE_TEXT", "UF_ICON_CLASS"]
            );

            //echo microtime()."--end--";
            while ($arSect = $rsSect->GetNext()) {
                if ($arSect['ELEMENT_CNT'] < 5) {
                    $prodList = CIBlockElement::GetList(
                        [],
                        ['SECTION_ID' => $arSect['ID'], 'CATALOG_AVAILABLE' => 'Y'],
                        false,
                        false,
                        ['ID']
                    );

                    if (!$availableProduct = $prodList->fetch()) {
                        continue;
                    }
                }  //278 запросов

                if ($arSect["DEPTH_LEVEL"] == 1) {
                    $allCatalog["CATALOG"][$arSect["ID"]]["NAME"] = $arSect["NAME"];
                    $allCatalog["CATALOG"][$arSect["ID"]]["SECTION_PAGE_URL"] = $arSect["SECTION_PAGE_URL"];
                    $allCatalog["CATALOG"][$arSect["ID"]]["SECTION_LEVEL_2"] = $arCatalogGroup["CATALOG_GROUP"];
                    $allCatalog["CATALOG"][$arSect["ID"]]["CODE"] = $arSect["CODE"];
                    $prevSect_1 = $arSect["ID"];
                    if ($arSect["UF_ICON_CLASS"]) {
                        $allCatalog["CATALOG"][$arSect["ID"]]["UF_ICON_CLASS"] = $arSect["UF_ICON_CLASS"];
                    }
                } elseif ($arSect["DEPTH_LEVEL"] == 2 && $arSect["UF_GROUP"]) {
                    $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$arSect["UF_GROUP"]]["SECTION_LEVEL_3"][$arSect["ID"]]["NAME"] = $arSect["NAME"];
                    $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$arSect["UF_GROUP"]]["SECTION_LEVEL_3"][$arSect["ID"]]["SECTION_PAGE_URL"] = $arSect["SECTION_PAGE_URL"];
                    $prevSect_2 = $arSect["ID"];
                    $prevUfGroup = $arSect["UF_GROUP"];
                    if ($arSect["PICTURE"]) {
                        $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$arSect["UF_GROUP"]]["SECTION_LEVEL_3"][$arSect["ID"]]["PICTURE"] = CFile::GetPath($arSect["PICTURE"]);
                    } //20 запросов
                    if ($arSect["UF_PICTURE_TEXT"]) {
                        $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$arSect["UF_GROUP"]]["SECTION_LEVEL_3"][$arSect["ID"]]["UF_PICTURE_TEXT"] = $arSect["UF_PICTURE_TEXT"];
                    }
                    // id подкатегорий
                    $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$arSect["UF_GROUP"]]["SECTION_LEVEL_3"][$arSect["ID"]]["ID_SUBSECTION"] = [];
                } elseif ($arSect["DEPTH_LEVEL"] == 3) {
                    if ($arSect["ELEMENT_CNT"] > 0) {
                        $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$prevUfGroup]["SECTION_LEVEL_3"][$prevSect_2]["SECTION_LEVEL_4"][$arSect["ID"]]["NAME"] = $arSect["NAME"];
                        $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$prevUfGroup]["SECTION_LEVEL_3"][$prevSect_2]["SECTION_LEVEL_4"][$arSect["ID"]]["COUNT_ELEMENT"] = $arSect["ELEMENT_CNT"];
                        $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$prevUfGroup]["SECTION_LEVEL_3"][$prevSect_2]["SECTION_LEVEL_4"][$arSect["ID"]]["SECTION_PAGE_URL"] = $arSect["SECTION_PAGE_URL"];
                    }
                    $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$prevUfGroup]["SECTION_LEVEL_3"][$prevSect_2]["ID_SUBSECTION"][] = $arSect["ID"];
                    $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$prevUfGroup]["SECTION_LEVEL_3"][$prevSect_2]["SHARED_ACTION"][] = $sharedAction[$arSect["ID"]];
                    $allCatalog["CATALOG"][$prevSect_1]["SECTION_LEVEL_2"][$prevUfGroup]["SECTION_LEVEL_3"][$prevSect_2]["BRAND"][] = $level3WithBrand[$arSect["ID"]];
                }
            }

            // дописываем в общий каталог данные о брендах и акциях
            foreach ($allCatalog["CATALOG"] as $id1 => &$section_level_1) {
                // рассчитываем количество строк в левом меню, правое не должно быть больше него
                $leftMenuHeight = array_reduce($section_level_1['SECTION_LEVEL_2'], function ($carry, $item) {
                    if (!is_array($item['SECTION_LEVEL_3'])) return $carry;
                    return $carry + 1 + count($item['SECTION_LEVEL_3']);
                }, 0);
                foreach ($section_level_1["SECTION_LEVEL_2"] as $id2 => &$section_level_2) {
                    // очистка пустых элементов
                    if (empty($section_level_2["SECTION_LEVEL_3"])) {
                        unset($section_level_1["SECTION_LEVEL_2"][$id2]);
                    }
                    foreach ($section_level_2["SECTION_LEVEL_3"] as $id3 => &$section_level_3) {
                        // перебираем бренды и суммируем одинаковые
                        $allBrandInSect = [];
                        foreach ($section_level_3["BRAND"] as $sect) {
                            foreach ($sect as $keyBrand => $brand) {
                                // проверяем есть ли в массиве, если есть плюсуем число товаров у бренда
                                if (array_key_exists($keyBrand, $allBrandInSect)) {
                                    $allBrandInSect[$keyBrand]["COUNT_ELEMENT"] = $allBrandInSect[$keyBrand]["COUNT_ELEMENT"] + $brand["COUNT_ELEMENT"];
                                } else {
                                    $allBrandInSect[$keyBrand] = $brand;
                                }
                            }
                        }
                        // выравниваем высоту подменю по левому меню
                        $allBrandInSect = array_slice($allBrandInSect, 0, $leftMenuHeight - 2, true);
                        $section_level_3["BRAND"] = $allBrandInSect;
                        $section_level_3["SECTION_LEVEL_4"] = array_slice($section_level_3['SECTION_LEVEL_4'], 0, $leftMenuHeight - 2, true);

                        //случайно выбираем одну акцию из списка
                        $allShareInSect = [];
                        foreach ($section_level_3["SHARED_ACTION"] as $sect) {
                            foreach ($sect as $keyShared => $share) {
                                $allShareInSect[] = $share;
                            }
                        }
                        $section_level_3["SHARED_ACTION"] = $allShareInSect[array_rand($allShareInSect)];
                    }
                }
            }

            /*
            // Добавляем бренды
            $allCatalog["CATALOG"][] = array(
                "NAME" => "Бренды",
                "CODE" => "brendy",
                "SECTION_PAGE_URL" => "/brendy/",
                "SECTION_LEVEL_2" => [],
                "UF_ICON_CLASS" => "icon-menu-brands",
            );
            */
            // Добавляем распродажу
            $allCatalog["CATALOG"][] = array(
                "NAME" => "Акции",
                "CODE" => "rasprodazha",
                "SECTION_PAGE_URL" => "/rasprodazha/",
                "SECTION_LEVEL_2" => [],
                "UF_ICON_CLASS" => "icon-menu-sale",
            );

            $this->arResult = $allCatalog;

            //////////// end cache /////////
            if ($cache_time > 0) {
                $cache->StartDataCache($cache_time, $cache_id, $cache_path);
                $cache->EndDataCache(array("menuResult" => $this->arResult));
            }
        }

        if (SITE_ID == 's1') {
            $template = 'template';
        } else {
            foreach ($this->arResult['CATALOG'] as $id => $section) {
                $sectionQuery = CIBlockSection::GetList([], ['SECTION_ID' => $id, '%NAME' => 'Ветер'])->Fetch();
                if ($sectionQuery) {
                    $this->arResult['CATALOG'][$id]['VET_APTEKA_NAME'] = $sectionQuery['NAME'];
                    $this->arResult['CATALOG'][$id]['VET_APTEKA_URL'] = $sectionQuery['CODE'];

                    unset($vetSections);
                    $sections = \CIBlockSection::GetTreeList(['IBLOCK_ID' => IBLOCK_CATALOG, 'SECTION_ID' => $sectionQuery['ID']]);
                    while ($info = $sections->GetNext()) {
                        $vetSections[] = $info['ID'];
                    }
                    if ($vetSections) {
                        $brands = $this->get_brands($vetSections);
                        $ids = [];
                        foreach ($brands as $brand) {
                            foreach ($brand as $array) {
                                if (!in_array($array['ID'], $ids)) {
                                    $this->arResult['CATALOG'][$id]['VET_APTEKA_BRANDS'][] = $array;
                                    $ids[] = $array['ID'];
                                }
                            }
                        }
                    }
                }
            }
            $template = 'vetapteka';
        }

        $this->IncludeComponentTemplate($template);
    }

    private function get_shared_action()
    {

        $sharedAction = [];
        $sharedActionId = [];

        //выбираем все акционные торговые предложения
        $arSelect = Array("ID", "NAME", "PROPERTY_WEIGHT", "CML2_LINK", "PROPERTY_CML2_LINK.PREVIEW_PICTURE", "PROPERTY_CML2_LINK.IBLOCK_SECTION_ID",
            "PROPERTY_CML2_LINK.NAME", "PROPERTY_NEW", "PROPERTY_BESTSELLER", "PROPERTY_SHARED_ACTION", "PROPERTY_CML2_LINK.ID",
            "PROPERTY_CML2_LINK.DETAIL_PAGE_URL");
        $arFilter = Array(
            "IBLOCK_CODE" => 'catalog_trade_offers',
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y",
            "PROPERTY_DISPLAY_IN_MENU_VALUE" => "Y",
            "CATALOG_AVAILABLE" => "Y"
        );

        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNext()) {
            // если есть изображение - 591 запрос
            if ($ob["PROPERTY_CML2_LINK_PREVIEW_PICTURE"]) {
                $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][$ob["ID"]]["ID"] = $ob["ID"];
                $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][$ob["ID"]]["NAME"] = $ob["PROPERTY_CML2_LINK_NAME"];
                $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][$ob["ID"]]["WEIGHT"] = $ob["PROPERTY_WEIGHT_VALUE"];
                $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][$ob["ID"]]["PICTURE"] = CFile::GetPath($ob["PROPERTY_CML2_LINK_PREVIEW_PICTURE"]);
                $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][$ob["ID"]]["SECTION"] = $ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"];
                $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][$ob["ID"]]["NEW"] = $ob["PROPERTY_NEW_VALUE"];
                $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][$ob["ID"]]["BESTSELLER"] = $ob["PROPERTY_BESTSELLER_VALUE"];
                $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][$ob["ID"]]["SHARED_ACTION"] = $ob["PROPERTY_SHARED_ACTION_VALUE"];
                $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][$ob["ID"]]["DETAIL_PAGE_URL"] =
                    str_replace('//', '/', $ob["PROPERTY_CML2_LINK_DETAIL_PAGE_URL"]);
                $sharedActionId[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][] = $ob["ID"];
            }
            //71 запрос
            if (!empty($ob["PROPERTY_CML2_LINK_ID"])) {
                $db_groups = CIBlockElement::GetElementGroups($ob["PROPERTY_CML2_LINK_ID"], true);
                while ($ar_group = $db_groups->Fetch()) {
                    if ($ar_group['ID'] != $ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"] && empty($sharedAction[$ar_group['ID']])) {
                        //$sharedAction[$ar_group['ID']] = $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]];
                        $sharedAction[$ar_group['ID']][$ob['ID']] = $sharedAction[$ob["PROPERTY_CML2_LINK_IBLOCK_SECTION_ID"]][$ob['ID']];
                        $sharedActionId[$ar_group['ID']][] = $ob['ID'];
                    }
                }
            }
        }

        //расчет цен для всех вариантов - 798 запросов
        $cachePrices = array();
        foreach ($sharedActionId as $idSection => $section) {
            foreach ($section as $id) {
                if (empty($cachePrices[$id])) {
                    $arPrice = CCatalogProduct::GetOptimalPrice($id);
                    $cachePrices[$id] = $arPrice;
                } else {
                    $arPrice = $cachePrices[$id];
                }
                if (is_array($arPrice['RESULT_PRICE'])) {
                    $sharedAction[$idSection][$id]["PRICE"] = $arPrice["RESULT_PRICE"];
                } else {
                    unset($sharedAction[$idSection][$id]);
                }
            }
        }
        return $sharedAction;
    }

    private function get_brands($sectionIds = null)
    {
        //выбираем все бренды в разрезе категорий

//        $brands = [];
//        $arSelect = Array("IBLOCK_SECTION_ID", "PROPERTY_BRAND", "PROPERTY_BRAND.NAME", "PROPERTY_BRAND.DETAIL_PAGE_URL", "PROPERTY_BRAND.CODE", "ID", "IBLOCK_ID");
//        $arFilter = Array("IBLOCK_CODE" => 'catalog', "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
//        if ($sectionIds) {
//            $arFilter['SECTION_ID'] = $sectionIds;
//        }

        //6197 запросов!!! оптимизировать в первую очередь!!!
        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
//        $res = CIBlockElement::GetList(Array('ID' => 'ASC'), $arFilter, false, false, $arSelect);
//        while ($ob = $res->GetNext()) {
//            if ($ob["PROPERTY_BRAND_NAME"]) {
//                $rsGroups = CIBlockElement::GetElementGroups($ob['ID'], true, ['ID']);
//                while ($arGroup = $rsGroups->Fetch()) {
//                    $brands[$arGroup['ID']][$ob["PROPERTY_BRAND_VALUE"]]["ID"] = $ob["PROPERTY_BRAND_VALUE"];
//                    $brands[$arGroup['ID']][$ob["PROPERTY_BRAND_VALUE"]]["COUNT_ELEMENT"] = $brands[$arGroup['ID']][$ob["PROPERTY_BRAND_VALUE"]]["COUNT_ELEMENT"] + 1;
//                    $brands[$arGroup['ID']][$ob["PROPERTY_BRAND_VALUE"]]["NAME"] = $ob["PROPERTY_BRAND_NAME"];
//                    $brands[$arGroup['ID']][$ob["PROPERTY_BRAND_VALUE"]]["CODE"] = $ob["PROPERTY_BRAND_CODE"];
//                    $brands[$arGroup['ID']][$ob["PROPERTY_BRAND_VALUE"]]["DETAIL_PAGE_URL"] = $ob["PROPERTY_BRAND_DETAIL_PAGE_URL"];
//                }
//            }
//        }
//        echo "<pre>"; print_r(count($brands)); echo "</pre>----";
//        echo "old<pre>"; print_r($brands); echo "</pre>old";

        // сократил до 3 запросов!!!
        $brands = [];
        $ids = [];
        $brandValues = [];
        $brandCodes = [];
        $names = [];
        $detail_page_urls = [];

        $arSelect = Array("IBLOCK_SECTION_ID", "PROPERTY_BRAND", "PROPERTY_BRAND.NAME", "PROPERTY_BRAND.DETAIL_PAGE_URL", "PROPERTY_BRAND.CODE", "ID", "IBLOCK_ID");
        $arFilter = Array("IBLOCK_CODE" => 'catalog', "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
        if ($sectionIds) {
            $arFilter['SECTION_ID'] = $sectionIds;
        }

        $res = CIBlockElement::GetList(Array('ID' => 'ASC'), $arFilter, false, false, $arSelect);
        $key = 0;
        while ($ob = $res->GetNext()) {
            if ($ob["PROPERTY_BRAND_NAME"]) {
                $ids[$key] = $ob['ID'];
                $brandValues[$key][$ob['ID']] = $ob['PROPERTY_BRAND_VALUE'];
                $brandCodes[$key][$ob['ID']] = $ob['PROPERTY_BRAND_CODE'];
                $names[$key][$ob['ID']] = $ob['PROPERTY_BRAND_NAME'];
                $detail_page_urls[$key][$ob['ID']] = $ob['PROPERTY_BRAND_DETAIL_PAGE_URL'];
                $key++;
            }
        }


        $rsGroups = CIBlockElement::GetElementGroups($ids, true, ['ID', 'IBLOCK_ELEMENT_ID']);
        while ($arGroup = $rsGroups->Fetch()){
            $key = array_search($arGroup['IBLOCK_ELEMENT_ID'], $ids);
            $brands[$arGroup['ID']][$brandValues[$key][$arGroup['IBLOCK_ELEMENT_ID']]]["ID"] = $brandValues[$key][$arGroup['IBLOCK_ELEMENT_ID']];
            $brands[$arGroup['ID']][$brandValues[$key][$arGroup['IBLOCK_ELEMENT_ID']]]["COUNT_ELEMENT"] = $brands[$arGroup['ID']][$brandValues[$key][$arGroup['IBLOCK_ELEMENT_ID']]]["COUNT_ELEMENT"] + 1;
            $brands[$arGroup['ID']][$brandValues[$key][$arGroup['IBLOCK_ELEMENT_ID']]]["NAME"] = $names[$key][$arGroup['IBLOCK_ELEMENT_ID']];
            $brands[$arGroup['ID']][$brandValues[$key][$arGroup['IBLOCK_ELEMENT_ID']]]["CODE"] = $brandCodes[$key][$arGroup['IBLOCK_ELEMENT_ID']];
            $brands[$arGroup['ID']][$brandValues[$key][$arGroup['IBLOCK_ELEMENT_ID']]]["DETAIL_PAGE_URL"] = $detail_page_urls[$key][$arGroup['IBLOCK_ELEMENT_ID']];
        }

        return $brands;
    }
}