Кастомный компонент стороннего разработчика для главного меню сайта на 1С-Битрикс. 
В файле class.php в функции get_brands() приведен пример, как я оптимизировал функцию,
 убрав лишние запросы к БД в цикле. Строки 283 и ниже (старый код до моих исправлений
 закомментирован и сохранен для сравнения, что именно я там изменил).
 Примечание - в файле масса другого неоптимального кода, он унаследованный и с ним я не
 работал, пример касается только одной этой функции, как самой нагружающей
