<?foreach ($arResult["CATALOG"] as $id => $section):?>
    <?if ($section['VET_APTEKA_URL']):?>
        <a href="<?= $section["SECTION_PAGE_URL"] . $section['VET_APTEKA_URL'];?>/" class="main-menu__item"
           data-menu-item="<?= $section["CODE"]; ?>">
            <span class="icon <?= $section["UF_ICON_CLASS"]; ?>"></span> <?= $section["NAME"]; ?>
        </a>
    <?endif;?>
    <div class="main-menu__drop js-drop-menu" data-menu-drop="<?= $section["CODE"]; ?>">
        <div class="drop-menu-left">
            <div class="drop-menu-left__block">
                <? if ($showAllProducts):
                    $showAllProducts = false;
                    ?>
                    <a href="<?= $section['SECTION_PAGE_URL'] . $section['VET_APTEKA_URL'];?>/" class="drop-menu-left__common">Все
                        товары</a>
                <? endif; ?>
                <h3 class="drop-menu-left__header"><?= $section['VET_APTEKA_NAME'];?></h3>
                <ul class="drop-menu-left__submenu">  
                        <li>
                            <a href="<?= $section_level_3["SECTION_PAGE_URL"]; ?>"
                               class="drop-menu-left__link">
                                <span class="text-underline"><?= $section['VET_APTEKA_NAME'];?></span>
                            </a>
                            <div class="drop-menu-right">
                                <div class="drop-menu-right__block">
                                    <h3 class="drop-menu-right__header">Особенности</h3>
                                    <a href="<?= $section["SECTION_PAGE_URL"] . $section['VET_APTEKA_URL'];?>/"
                                       class="drop-menu-right__see-all">Все товары</a>
                                    <span class="icon icon-more"></span>
                                </div>
                                <?if ($section["VET_APTEKA_BRANDS"]):?>
                                    <div class="drop-menu-right__block">
                                        <h3 class="drop-menu-right__header">Бренды</h3>
                                        <ul class="drop-menu-right__list">
                                            <?foreach ($section["VET_APTEKA_BRANDS"] as $id => $brand):?>
                                                <li>
                                                    <a href="<?= $section["SECTION_PAGE_URL"] . $brand["CODE"] ?>/"
                                                       class="drop-menu-right__link"><?= $brand["NAME"] ?></a>
                                                    <span class="drop-menu-right__count">(<?= $brand["COUNT_ELEMENT"] ?>)</span>
                                                </li>
                                            <?endforeach;?>
                                        </ul>
                                        <a href="/brendy/" class="drop-menu-right__see-all">Все бренды</a>
                                        <span class="icon icon-more"></span>
                                    </div>
                                <?endif;?>
                            <div class="drop-menu-right__block">
                            	<?foreach ($section['SECTION_LEVEL_2'] as $section2):?>
                            		<?foreach ($section2['SECTION_LEVEL_3'] as $level3):?>
                            			<?if ($level3['PICTURE'] && !$picture):?>
                            				<figure class="drop-menu-right__image">
		                                        <img src="<?= $level3["PICTURE"]; ?>"
		                                             alt="<?= $level3["UF_PICTURE_TEXT"] ?>">
		                                        <?if ($level3["UF_PICTURE_TEXT"]):?>
		                                            <figcaption><?= $level3["UF_PICTURE_TEXT"]; ?></figcaption>
		                                        <?endif;?>
		                                    </figure>
		                                    <?$picture = true;?>
                        				<?endif;?>
                        			<?endforeach;?>
                        		<?endforeach;?>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?endforeach;?>