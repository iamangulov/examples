<? foreach ($arResult["CATALOG"] as $id => $section_level_1): ?>
    <? $showAllProducts = true; ?>
    <a href="<?= $section_level_1["SECTION_PAGE_URL"]; ?>" class="main-menu__item"
       data-menu-item="<?= $section_level_1["CODE"]; ?>">
        <span class="icon <?= $section_level_1["UF_ICON_CLASS"]; ?>"></span> <?= $section_level_1["NAME"]; ?>
    </a>
    <? if (!empty($section_level_1["SECTION_LEVEL_2"])): ?>
        <div class="main-menu__drop js-drop-menu" data-menu-drop="<?= $section_level_1["CODE"]; ?>">
            <div class="drop-menu-left">
                <? foreach ($section_level_1["SECTION_LEVEL_2"] as $id => $section_level_2) { ?>
                    <div class="drop-menu-left__block">
                        <? if ($showAllProducts):
                            $showAllProducts = false;
                            ?>
                            <a href="<?= $section_level_1['SECTION_PAGE_URL'] ?>" class="drop-menu-left__common">Все
                                товары</a>
                        <? endif; ?>
                        <h3 class="drop-menu-left__header"><?= $section_level_2["NAME"]; ?></h3>
                        <ul class="drop-menu-left__submenu">
                            <? foreach ($section_level_2["SECTION_LEVEL_3"] as $id3 => $section_level_3) { ?>
                                <? if ($section_level_3["SECTION_LEVEL_4"] || $section_level_3["BRAND"]) { ?>
                                    <li>
                                        <a href="<?= $section_level_3["SECTION_PAGE_URL"]; ?>"
                                           class="drop-menu-left__link">
                                            <span class="text-underline"><?= $section_level_3["NAME"]; ?></span>
                                        </a>
                                        <div class="drop-menu-right">
                                            <div class="drop-menu-right__block">
                                                <h3 class="drop-menu-right__header">Особенности</h3>
                                                <ul class="drop-menu-right__list">
                                                    <? foreach ($section_level_3["SECTION_LEVEL_4"] as $id => $section_level_4) { ?>
                                                        <li>
                                                            <a href="<?= $section_level_4["SECTION_PAGE_URL"]; ?>"
                                                               class="drop-menu-right__link"><?= $section_level_4["NAME"]; ?></a>
                                                            <span class="drop-menu-right__count">(<?= $section_level_4["COUNT_ELEMENT"]; ?>)</span>
                                                        </li>
                                                    <? } ?>
                                                </ul>
                                                <a href="<?= $section_level_3["SECTION_PAGE_URL"]; ?>"
                                                   class="drop-menu-right__see-all">Все товары</a>
                                                <span class="icon icon-more"></span>
                                            </div>
                                            <? if ($section_level_3["BRAND"]) { ?>
                                                <div class="drop-menu-right__block">
                                                    <h3 class="drop-menu-right__header">Бренды</h3>
                                                    <ul class="drop-menu-right__list">
                                                        <? foreach ($section_level_3["BRAND"] as $id => $brand) { ?>
                                                            <li>
                                                                <a href="<?= $section_level_3["SECTION_PAGE_URL"] . $brand["CODE"] ?>/"
                                                                   class="drop-menu-right__link"><?= $brand["NAME"] ?></a>
                                                                <span class="drop-menu-right__count">(<?= $brand["COUNT_ELEMENT"] ?>)</span>
                                                            </li>
                                                        <? } ?>
                                                    </ul>
                                                    <a href="/brendy/" class="drop-menu-right__see-all">Все бренды</a>
                                                    <span class="icon icon-more"></span>
                                                </div>
                                            <? } ?>
                                            <div class="drop-menu-right__block">
                                                <? if ($section_level_3["SHARED_ACTION"]) { ?>
                                                    <div class="drop-menu-right__sale">
                                                        <img src="<?= $section_level_3["SHARED_ACTION"]["PICTURE"] ?>"
                                                             class="pull-left" alt="">
                                                        <? if ($section_level_3["SHARED_ACTION"]["NEW"]): ?>
                                                            <p class="sticker sticker_red">Новинка</p>
                                                        <? elseif ($section_level_3["SHARED_ACTION"]["BESTSELLER"]): ?>
                                                            <p class="sticker sticker_purple">Хит продаж</p>
                                                        <? elseif ($section_level_3["SHARED_ACTION"]["SHARED_ACTION"]): ?>
                                                            <p class="sticker sticker_orange">Акция</p>
                                                        <? endif ?>
                                                        <div class="drop-menu-right__sale-info">
                                                            <a href="<?= $section_level_3["SHARED_ACTION"]["DETAIL_PAGE_URL"] ?>"><?= $section_level_3["SHARED_ACTION"]["NAME"]; ?></a>
                                                            <span class="drop-menu-right__desc"><?= ($section_level_3["SHARED_ACTION"]["WEIGHT"] ? $section_level_3["SHARED_ACTION"]["WEIGHT"] : ""); ?></span>
                                                            <? if ($section_level_3["SHARED_ACTION"]["PRICE"]["BASE_PRICE"] != $section_level_3["SHARED_ACTION"]["PRICE"]["DISCOUNT_PRICE"]) { ?>
                                                                <span class="drop-menu-right__old-price"><?= number_format($section_level_3["SHARED_ACTION"]["PRICE"]["BASE_PRICE"], 0, ',', ' ') ?>
                                                                    руб.</span>
                                                            <? } ?>
                                                            <? if ($section_level_3["SHARED_ACTION"]["PRICE"]["DISCOUNT_PRICE"]) { ?>
                                                                <span class="drop-menu-right__price"><?= number_format($section_level_3["SHARED_ACTION"]["PRICE"]["DISCOUNT_PRICE"], 0, ',', ' ') ?>
                                                                    руб.</span>
                                                            <? } ?>
                                                        </div>
                                                    </div>
                                                <? } ?>

                                                <? if ($section_level_3["PICTURE"]) { ?>
                                                    <figure class="drop-menu-right__image">
                                                        <img src="<?= $section_level_3["PICTURE"]; ?>"
                                                             alt="<?= $section_level_3["UF_PICTURE_TEXT"] ?>">
                                                        <? if ($section_level_3["UF_PICTURE_TEXT"]) { ?>
                                                            <figcaption><?= $section_level_3["UF_PICTURE_TEXT"]; ?></figcaption>
                                                        <? } ?>
                                                    </figure>
                                                <? } ?>
                                            </div>
                                        </div>
                                    </li>
                                <? } ?>
                            <? } ?>
                        </ul>
                    </div>
                <? } ?>
            </div>
        </div>
    <? endif; ?>
<? endforeach; ?>