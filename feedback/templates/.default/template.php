<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?//dmp($arParams);?>
<script>
    window["<?='on_'.$arParams['FORM_ID']?>"] =
        function (token) {
            $("<?='#'.$arParams['FORM_ID'];?>").submit(function (e) {
                e.preventDefault();

                var url = "<?=$this->GetFolder().'/ajax.php'?>";

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $(this).serialize(),
                    success: function (data) {
                        $("<?='#response_'.$arParams['FORM_ID'];?>").text(data);
                    }
                });

            });
            $("<?='#'.$arParams['FORM_ID'];?>").submit();
            grecaptcha.reset();
        };


</script>
<div class="section-feedback">
    <div class="section-feedback__head">
        <div class="b-wrapper">
            <h6 class="title-xm font-bold text-uppercase">
                <?=$arParams["HEAD"];?>
            </h6>
        </div>
    </div>
    <div class="section-feedback__body section-cover" style="background-image:url(<?=SITE_TEMPLATE_PATH.$arParams['BACKGROUND_IMG']?>);">
        <div class="b-wrapper">
                <h4 class="title-xm font-bold text-center" id="response_<?=$arParams["FORM_ID"];?>">
                    <?=$arParams["HEAD_INNER"];?>
                </h4>
            <div class="line-sm">
                <div class="section-feedback__cols" id="bp1">

                    <form  id="<?=$arParams["FORM_ID"];?>" name="<?=$arParams["FORM_ID"];?>" action="" method="POST" enctype="multipart/form-data" class="contact-form" novalidate="novalidate">
                        <?=bitrix_sessid_post()?>
                        <input type="hidden" value="<?=$arParams["EVENT_ID"];?>" name="event">
                        <input type="hidden" value="<?=$arParams["IBLOCKS"][0];?>" name="iblock">
                        <input type="hidden" value="<?=$arParams["HEAD_INNER"];?>" name="form_head">
                        <input type="hidden" value="<?=$APPLICATION->GetCurPage();?>" name="page">
                        <input type="hidden" value="<?=$arParams["EVENT_MESSAGE_ID"][0];?>" name="message_id">
                        <?if(isset($arParams["USE_FIELD_NAME"]) && $arParams["USE_FIELD_NAME"] == "Y"):?>
                        <div class="section-feedback__col _sm">
                            <input name="name" type="text" class="field text-center" value="" placeholder="Ваше имя">
                        </div>
                        <?endif;?>
                        <?if(isset($arParams["USE_FIELD_EMAIL"]) && $arParams["USE_FIELD_EMAIL"] == "Y"):?>
                        <div class="section-feedback__col _sm">
                            <input name="email" type="email" class="field text-center" value="" placeholder="Ваш e-mail">
                        </div>
                        <?endif;?>
                        <?if(isset($arParams["USE_FIELD_PHONE"]) && $arParams["USE_FIELD_PHONE"] == "Y"):?>
                        <div class="section-feedback__col _sm">
                            <input name="phone" type="phone" class="field text-center" value="" placeholder="Ваш телефон">
                        </div>
                        <?endif;?>
                        <?if(isset($arParams["USE_FIELD_SELECT"]) && $arParams["USE_FIELD_SELECT"] == "Y"):?>
                        <div class="section-feedback__col _sm">
                            <select class="inputselect valid field text-center" name="service">
                                <option value="development">Разработка</option>
                                <option value="promotion">Продвижение</option>
                                <option value="advertising">Интернет-Реклама</option>
                            </select>
                        </div>
                        <?endif;?>
                        <?if(isset($arParams["USE_FIELD_MESSAGE"]) && $arParams["USE_FIELD_MESSAGE"] == "Y"):?>
                            <div class="section-feedback__col _sm">
                                <textarea name="message" class="field text-center" value="" placeholder="Ваше сообщение"></textarea>
                            </div>
                        <?endif;?>
                        <div class="section-feedback__col">
                                                        <input data-sitekey="ВашКлючСайта" data-callback='on_<?=$arParams["FORM_ID"]?>' class="g-recaptcha button-reset button-base _size-s button-block" type="submit" name="web_form_submit" value="<?=$arParams["BUTTON_TEXT"];?>" />

<!--                            <input  class="button-reset button-base _size-s button-block" type="submit" name="web_form_submit" value="Хочу подробностей" />-->
                        </div>
                        <div class="line-smm paragraph-small">
                            <input name="agree" type="checkbox" required="required" checked="checked" value="true" class="flag _left" form="<?=$arParams["FORM_ID"];?>">
                            <a href="/agree/" target="_blank" class="agreement_a" style="color: white;">Я согласен на обработку моих персональных данных</a>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(function(){
        $('input[type="phone"]').mask('+7 (999) 999-99-99');
    });
</script>
