<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?

//dmp($_POST);

if ((isset($_POST['name'])) && (empty($_POST['name']) || $_POST['name']== 'Ваше имя')) {
    echo "Введите имя";
    exit;
}

if (isset($_POST['email']) && (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))) {
    echo "Введите правильный email";
    exit;
}

if (isset($_POST['phone']) && (empty($_POST['phone']))) {
    echo "Введите телефон";
    exit;
}

if (isset($_POST['service']) && (empty($_POST['service']))) {
    echo "Выберите услугу";
    exit;
}


if (isset($_POST['message']) && (empty($_POST['message']))) {
    echo "Введите сообщение";
    exit;
}

if (!isset($_POST['agree'])) {
    echo "Необходимо согласие на обработку данных";
    exit;
}

if(CEvent::Send($_POST["event"], 's1', $_POST, "N", $_POST["message_id"]))
{
    if(!CModule::IncludeModule('iblock')){}

    $el = new CIBlockElement();

    $PROP = array();
    $PROP['NAME'] = $_POST['name'];
    $PROP['EMAIL'] = $_POST['email'];
    $PROP['PHONE'] = $_POST['phone'];
    $PROP['SERVICE'] = $_POST['service'];
    $PROP['MESSAGE'] = $_POST['message'];

    $arLoadProductArray = Array(
        "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
        "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
        "IBLOCK_ID"      => $_POST['iblock'],
        "PROPERTY_VALUES"=> $PROP,
        "NAME"           => "На странице ".$_POST['page']." заполнена форма ".$_POST['form_head'],
        "ACTIVE"         => "Y",
    );

    if($PRODUCT_ID = $el->Add($arLoadProductArray))
    {};
}

echo "Сообщение отправлено";

?>