<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if($arCurrentValues["EVENT_ID"]){
    $arFilter = Array("TYPE_ID" => $arCurrentValues["EVENT_ID"], "ACTIVE" => "Y");
}else{
    $arFilter = Array("ACTIVE" => "Y");
}
$arEvent = Array();
$dbType = CEventMessage::GetList($by="ID", $order="DESC", $arFilter);
while($arType = $dbType->GetNext())
    $arEvent[$arType["ID"]] = "[".$arType["ID"]."] ".$arType["SUBJECT"];

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
        'CACHE_TIME' => array('DEFAULT' => 120),
        "HEAD" => array(
            "NAME" => GetMessage("HEAD"),
            "TYPE" => "STRING",
            "DEFAULT" => "Хотите заказать РАЗРАБОТКУ ИНТЕРНЕТ-МАГАЗИНА? еСТЬ ВОПРОСЫ? ЗАПОЛНИТЕ ФОРМУ НИЖЕ И НАШИ СПЕЦИАЛИСТЫ С ВАМИ СВЯЖУТСЯ",
        ),
        "HEAD_INNER" => array(
            "NAME" => GetMessage("HEAD_INNER"),
            "TYPE" => "STRING",
            "DEFAULT" => "ЗАКАЗАТЬ КОНСУЛЬТАЦИЮ",
        ),
        "BACKGROUND_IMG" => array(
            "NAME" => GetMessage("MFP_BACKGROUND_IMG"),
            "TYPE" => "STRING",
            "DEFAULT" => "/img/photo_cover_02.jpg",
        ),
        "FORM_ID" => array(
            "NAME" => GetMessage("FORM_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "feedback",
        ),
        "EVENT_ID" => Array(
            "NAME" => GetMessage("MFP_EMAIL_EVENT"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
            "PARENT" => "BASE",
        ),
        "EVENT_MESSAGE_ID" => Array(
            "NAME" => GetMessage("MFP_EMAIL_TEMPLATES"),
            "TYPE"=>"LIST",
            "VALUES" => $arEvent,
            "DEFAULT"=>"",
            "MULTIPLE"=>"Y",
            "COLS"=>25,
            "PARENT" => "BASE",
        ),
        "BUTTON_TEXT" => Array(
            "NAME" => GetMessage("MFP_BUTTON_TEXT"),
            "TYPE" => "STRING",
            "DEFAULT" => "Хочу подробностей",
            "PARENT" => "BASE",
        ),
        "USE_FIELD_NAME" => Array(
            "NAME" => GetMessage("MFP_USE_FIELD_NAME"),
            "TYPE" => "CHECKBOX",
            "MULTIPLE" => "N",
            "VALUE" => "Y",
            "DEFAULT" =>"N",
            "REFRESH"=> "Y",
            "PARENT" => "BASE",
        ),
        "USE_FIELD_EMAIL" => Array(
            "NAME" => GetMessage("MFP_USE_FIELD_EMAIL"),
            "TYPE" => "CHECKBOX",
            "MULTIPLE" => "N",
            "VALUE" => "Y",
            "DEFAULT" =>"N",
            "REFRESH"=> "Y",
            "PARENT" => "BASE",
        ),
        "USE_FIELD_PHONE" => Array(
            "NAME" => GetMessage("MFP_USE_FIELD_PHONE"),
            "TYPE" => "CHECKBOX",
            "MULTIPLE" => "N",
            "VALUE" => "Y",
            "DEFAULT" =>"N",
            "REFRESH"=> "Y",
            "PARENT" => "BASE",
        ),
        "USE_FIELD_SELECT" => Array(
            "NAME" => GetMessage("MFP_USE_FIELD_SELECT"),
            "TYPE" => "CHECKBOX",
            "MULTIPLE" => "N",
            "VALUE" => "Y",
            "DEFAULT" =>"N",
            "REFRESH"=> "Y",
            "PARENT" => "BASE",
        ),
        "USE_FIELD_MESSAGE" => Array(
            "NAME" => GetMessage("MFP_USE_FIELD_MESSAGE"),
            "TYPE" => "CHECKBOX",
            "MULTIPLE" => "N",
            "VALUE" => "Y",
            "DEFAULT" =>"N",
            "REFRESH"=> "Y",
            "PARENT" => "BASE",
        ),
        "SAVE_FORM_DATA" => Array(
            "NAME" => GetMessage("MFP_SAVE_FORM_DATA"),
            "TYPE" => "CHECKBOX",
            "MULTIPLE" => "N",
            "VALUE" => "Y",
            "DEFAULT" =>"N",
            "REFRESH"=> "Y",
            "PARENT" => "BASE",
        ),
	),
);

if($arCurrentValues["SAVE_FORM_DATA"] == "Y"){
    if(!CModule::IncludeModule("iblock"))
        return;

    $arTypesEx = CIBlockParameters::GetIBlockTypes();

    $arComponentParameters["PARAMETERS"]["IBLOCK_TYPE"] =  Array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_IBLOCK_DESC_LIST_TYPE"),
        "TYPE" => "LIST",
        "VALUES" => $arTypesEx,
        "DEFAULT" => "feedbacks",
        "REFRESH" => "Y",
    );

    $arIBlocks = Array();
    $db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
    while($arRes = $db_iblock->Fetch())
    {
        $arIBlocks[$arRes["ID"]] = $arRes["NAME"];
    }

    $arComponentParameters["PARAMETERS"]["IBLOCKS"]  =  Array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_IBLOCK_DESC_LIST_ID"),
        "TYPE" => "LIST",
        "VALUES" => $arIBlocks,
        "DEFAULT" => '',
        "MULTIPLE" => "Y",
    );

}
?>