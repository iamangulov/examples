<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$MESS["HEAD"] = "Заголовок перед формой";
$MESS["HEAD_INNER"] = "Заголовок внутри формы";
$MESS["FORM_ID"] = "ID формы (при размещении нескольких форм на странице задавать уникальным)";
$MESS ['MFP_SAVE_FORM_DATA'] = "Сохранять полученные данные в инфоблоке";
$MESS ['T_IBLOCK_DESC_LIST_TYPE'] = "Тип информационного блока";
$MESS ['T_IBLOCK_DESC_LIST_ID'] = "Код информационного блока";
$MESS ['MFP_EMAIL_EVENT'] = "Тип почтового события для отправки письма";
$MESS ['MFP_EMAIL_TEMPLATES'] = "Почтовые шаблоны для отправки письма";
$MESS ['MFP_POST_ID'] = "Номер почтового события для отправки письма";
$MESS ['MFP_BUTTON_TEXT'] = "Текст в кнопке отправки формы";
$MESS ['MFP_USE_FIELD_NAME'] = "Использовать поле 'Имя'";
$MESS ['MFP_USE_FIELD_EMAIL'] = "Использовать поле 'Email'";
$MESS ['MFP_USE_FIELD_PHONE'] = "Использовать поле 'Телефон'";
$MESS ['MFP_USE_FIELD_SELECT'] = "Использовать поле типа select (для специальных шаблонов)";
$MESS ['MFP_USE_FIELD_MESSAGE'] = "Использовать поле 'Сообщение' (для специальных шаблонов)";
$MESS ['MFP_BACKGROUND_IMG'] = "Картинка фона (путь от корня текущего шаблона сайта)";
?>