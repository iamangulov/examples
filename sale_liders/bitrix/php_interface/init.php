<?

//добавление пункта адм.меню Лидеры заказов
AddEventHandler('main', 'OnBuildGlobalMenu', 'AddToGlobalMenu');
function AddToGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
{
    foreach ($aModuleMenu as &$module){
        if($module["items_id"] == "menu_order"){

            $module["items"][] = array(
                "url"         => "sale_liders.php?lang=".LANG,  // ссылка на пункте меню
                "text"        => 'Лидеры заказов',       // текст пункта меню
                "title"       => 'Поиск заказчиков по числу сделанных заказов', // текст всплывающей подсказки
                "more_url" => array()
            );
        }
    }
}
