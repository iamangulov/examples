<?
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global string $DBType */
/** @global CDatabase $DB */

use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
//use Bitrix\Main\Config\Option;
//use Bitrix\Sale\Internals\OrderTable;
//use Bitrix\Sale;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loader::includeModule('sale');
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/prolog.php");

// include functions
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/general/admin_tool.php");

$saleModulePermissions = $APPLICATION->GetGroupRight("sale");

if($saleModulePermissions == "D")
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

$sTableID = "tbl_customers_on_params"; // ID таблицы
$oSort = new CAdminSorting($sTableID, "ID", "desc"); // объект сортировки
$lAdmin = new CAdminList($sTableID, $oSort); // основной объект списка

// проверку значений фильтра для удобства вынесем в отдельную функцию
function CheckFilter()
{
    global $FilterArr, $lAdmin;
    foreach ($FilterArr as $f) global $$f;

    // В данном случае проверять нечего.
    // В общем случае нужно проверять значения переменных $find_имя
    // и в случае возниконовения ошибки передавать ее обработчику
    // посредством $lAdmin->AddFilterError('текст_ошибки').

    return count($lAdmin->arFilterErrors) == 0; // если ошибки есть, вернем false;
}

// опишем элементы фильтра
$FilterArr = Array(
    "find_date_from",
    "find_date_to",
    "find_min_orders_count",
);

// инициализируем фильтр
$lAdmin->InitFilter($FilterArr);

// если все значения фильтра корректны, обработаем его
if (CheckFilter())
{
    // создадим массив фильтрации заказов по полям фильтра
    $arFilter = array(
        "!CANCELED" => "Y",
    );
    if(strval(trim($filter_date_from)) != '')
    {
        $arFilter[">=DATE_INSERT"] = trim($filter_date_from);
    }
    if(strval(trim($filter_date_to)) != '')
    {
        if($arDate = ParseDateTime($filter_date_to, CSite::GetDateFormat("FULL", SITE_ID)))
        {
            if(StrLen($filter_date_to) < 11)
            {
                $arDate["HH"] = 23;
                $arDate["MI"] = 59;
                $arDate["SS"] = 59;
            }

            $filter_date_to = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID)), mktime($arDate["HH"], $arDate["MI"], $arDate["SS"], $arDate["MM"], $arDate["DD"], $arDate["YYYY"]));
            $arFilter["<=DATE_INSERT"] = $filter_date_to;
        }
        else
        {
            $filter_date_to = "";
        }
    }
    if(strval(trim($find_min_orders_count)) != ''){
        $arFilter[">=CNT"] = trim($find_min_orders_count);
    }
}

// ******************************************************************** //
//                ВЫБОРКА ЭЛЕМЕНТОВ СПИСКА                              //
// ******************************************************************** //

//поля заказов
//$fileds = \Bitrix\Sale\Internals\OrderTable::getMap();
//dmp($fileds);

//получаем заказы, сгруппированные по пользователями с количеством заказов каждого пользователя
$dbOrderList = \Bitrix\Sale\Internals\OrderTable::getList(array(
    'order' => array("CNT" => "DESC"),
    'filter' => $arFilter,
    'group' => array("USER_ID"),
    'select' => array("USER_ID", "CNT"),
    'runtime' => array(
        new Entity\ExpressionField('CNT', 'COUNT(*)'),
    )
));

//$arOrderList = $dbOrderList->fetchAll();
//dmp($arOrderList);

// преобразуем список в экземпляр класса CAdminResult
$rsData = new CAdminResult($dbOrderList, $sTableID);

// аналогично CDBResult инициализируем постраничную навигацию.
$rsData->NavStart();

// отправим вывод переключателя страниц в основной объект $lAdmin
$lAdmin->NavText($rsData->GetNavPrint("Заказчики"));



// ******************************************************************** //
//                ПОДГОТОВКА СПИСКА К ВЫВОДУ                            //
// ******************************************************************** //

$lAdmin->AddHeaders(array(
    array(
        "id"    =>"ID",
        "content"  =>"",
        "sort"    =>"id",
        "align"    =>"right",
        "default"  =>true,
    ),
    array(
        "id"    =>"USER_ID",
        "content"  =>"Заказчик",
        "sort"    =>"user_id",
        "align"    =>"right",
        "default"  =>true,
    ),
    array(
        "id"    =>"PHONE",
        "content"  => "Телефон",
        "sort"    =>"phone",
        "align"    =>"right",
        "default"  =>true,
    ),
	array(
        "id"    =>"EMAIL",
        "content"  => "Email",
        "sort"    =>"email",
        "align"    =>"right",
        "default"  =>true,
    ),
    array(
        "id"    =>"CNT",
        "content"  => "Количество заказов",
        "sort"    =>"cnt",
        "align"    =>"right",
        "default"  =>true,
    ),

));

while($arRes = $rsData->NavNext(true, "f_")):
    // создаем строку. результат - экземпляр класса CAdminListRow
    $row =&$lAdmin->AddRow($f_ID, $arRes);

    $rsUser = CUser::GetByID($f_USER_ID);
    $arUser = $rsUser->Fetch();

    //номер последнего заказа пользователя
    $res = CSaleOrder::GetList(array("ID"=>"DESC"), Array("USER_ID" => $f_USER_ID), false, array("nTopCount" => 1), array("ID"));
    if($ob = $res->GetNext(false,false))
        $lastOrder = $ob["ID"];

    //телефон из последнего заказа
    $orderProps = CSaleOrderPropsValue::GetOrderProps($lastOrder);
    $phoneFromOrder = "";
    while ($props = $orderProps->fetch())
    {
        if($props['CODE'] == 'PHONE')
            $phoneFromOrder = $props['VALUE'];
    }
	
	//зачем дублировать телефоны
	$phone_print = $phoneFromOrder."<br />".$arUser["PERSONAL_MOBILE"];
	if($phoneFromOrder == $arUser["PERSONAL_MOBILE"])
		$phone_print = $phoneFromOrder;

    $row->AddViewField("USER_ID", '<a href="/bitrix/admin/user_edit.php?lang=ru&ID='.$f_USER_ID.'">'.$arUser["NAME"]." ".$arUser["LAST_NAME"].'</a> ');
	$row->AddViewField("PHONE", $phone_print);
	$row->AddViewField("EMAIL", $arUser["EMAIL"]);
    $row->AddViewField("CNT", $f_CNT);
endwhile;

// резюме таблицы
$lAdmin->AddFooter(
    array(
        array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()), // кол-во элементов
        array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"), // счетчик выбранных элементов
    )
);


// сформируем сброс фильтра
$aContext = array(
    array(
        "TEXT"=>"Сбросить фильтр",
        "LINK"=>"/bitrix/admin/sale_liders.php?lang=ru",
        "TITLE"=>"Сбросить фильтр",
        "ICON"=>"btn_new",
    ),
);

// и прикрепим его к списку
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                ВЫВОД                                                 //
// ******************************************************************** //

// альтернативный вывод
$lAdmin->CheckListMode();
// установим заголовок страницы
$APPLICATION->SetTitle('Лидеры заказов - поиск заказчиков по числу сделанных заказов');
// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// ******************************************************************** //
//                ВЫВОД ФИЛЬТРА                                         //
// ******************************************************************** //

// создадим объект фильтра
$oFilter = new CAdminFilter(
    $sTableID."_filter",
    array(
        "find_date_insert" => Loc::getMessage("SALE_F_DATE"),
        "find_min_orders_count" => "Минимальное кол-во заказов",
    )
);


?>
    <form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
        <?$oFilter->Begin();?>

        <tr>
            <td><b>Дата создания заказа:</b></td>
            <td>
                <?echo CalendarPeriod("filter_date_from", $filter_date_from, "filter_date_to", $filter_date_to, "find_form", "Y")?>
            </td>
        </tr>
        <tr>
            <td>Минимальное кол-во заказов</td>
            <td><input type="text" name="find_min_orders_count" size="4" value="<?echo htmlspecialchars($find_min_orders_count)?>"></td>
        </tr>


        <?
        $oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
        $oFilter->End();
        ?>


    </form>

<!--    <script>-->
<!--        var deleteButton = BX("tbl_customers_on_params_filterdel_filter");-->
<!--        BX.unbind(-->
<!--            deleteButton,-->
<!--            click,-->
<!--            function(){return tbl_customers_on_params_filter.OnClear('tbl_customers_on_params', '/bitrix/admin/sale_liders.php?lang=ru', this);}-->
<!--        );-->
<!--        BX.bind(deleteButton, "click", function() {window.location.href("http://salatniza.smartraf.ru/bitrix/admin/sale_liders.php?lang=ru"); });-->
<!---->
<!--    </script>-->

<?
$lAdmin->DisplayList();
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");